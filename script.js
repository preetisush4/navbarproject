
var app = angular.module('myApp', [
	'ngRoute',
	'ngMaterial',
  ])

  
app.controller("navbarController", function ($scope) {
	$scope.isNavVisible = false;
	$scope.toggleNav = function () {
	  $scope.isNavVisible = !$scope.isNavVisible;
	  console.log($scope.isNavVisible);
	};
  });

app.config(function($routeProvider) {
	$routeProvider
		
		.when('/', {
			templateUrl: 'main.html'
		})
});

angular.bootstrap(document.getElementById("root"), ["myApp"]);
